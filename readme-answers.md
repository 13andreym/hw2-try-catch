Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch


У випадку роботи з DOM, try...catch дозволяє обробляти помилки, які можуть виникнути при взаємодії з елементами сторінки, такі як неправильний селектор або відсутність очікуваного елементу.
Або при роботі з об'єктами/масивами  може бути використаний для захоплення помилок, які виникають при спробі отримати доступ до неіснуючої властивості об'єкта чи елемента масиву.
