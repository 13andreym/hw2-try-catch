const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

try {
  const root = document.getElementById('root');
  if (!root) {
    throw new Error('Element with id "root" not found.');
  }

  const ul = document.createElement('ul');
  
  books.forEach(book => {
    try {
      if (!book.author || !book.name || !book.price) {
        throw new Error(`Invalid book object: Missing property - ${!book.author ? 'author' : !book.name ? 'name' : 'price'}`);
      }

      const li = document.createElement('li');
      li.textContent = `"${book.name}" за авторством ${book.author} - Вартість: ${book.price || 'N/A'}`;
      ul.appendChild(li);
    } catch (error) {
      console.error('An error occurred:', error.message);
    }
  });

  root.appendChild(ul);
} catch (error) {
  console.error('An error occurred:', error.message);
}
